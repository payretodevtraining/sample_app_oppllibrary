<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Direct Debit (SEPA)</h3>
        </div>

        <div class="panel-body">
            <form action="<?php echo URL::to('settings', 'save'); ?>" class="form-horizontal" method="POST">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Enable?</label>

                    <div class="col-sm-10">
                        <div class="radio">
                            <label>
                                <input type="radio" name="OPP_SEPA_ENABLED" value="1"<?php echo $settings->get('OPP_SEPA_ENABLED') == 1 ? ' checked' : ''; ?>> Yes
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="OPP_SEPA_ENABLED" value="0"<?php echo $settings->get('OPP_SEPA_ENABLED') == 0 ? ' checked' : ''; ?>> No
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Server</label>

                    <div class="col-sm-10">
                        <div class="radio">
                            <label>
                                <input type="radio" name="OPP_SEPA_SERVER" value="1"<?php echo $settings->get('OPP_SEPA_SERVER') == 1 ? ' checked' : ''; ?>> Production
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="OPP_SEPA_SERVER" value="0"<?php echo $settings->get('OPP_SEPA_SERVER') == 0 ? ' checked' : ''; ?>> Test
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="OPP_SEPA_TRANSACTION_MODE" class="col-sm-2 control-label">Transaction Mode</label>

                    <div class="col-sm-10">
                        <select class="form-control" id="OPP_SEPA_TRANSACTION_MODE" name="OPP_SEPA_TRANSACTION_MODE">
                            <?php foreach ($transactionMode as $key => $mode): ?>
                                <option value="<?php echo $key; ?>"<?php echo $key == $settings->get('OPP_SEPA_TRANSACTION_MODE') ? ' selected' : ''; ?>><?php echo $mode; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="OPP_SEPA_ENTITY_ID" class="col-sm-2 control-label">Entity-ID</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="OPP_SEPA_ENTITY_ID" name="OPP_SEPA_ENTITY_ID" required value="<?php echo $settings->get('OPP_SEPA_ENTITY_ID'); ?>">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

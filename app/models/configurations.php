<?php

namespace App\Models;

require_once SYSTEM_DIR.'/collection.php';

use DB;
use System\Collection;

class Configurations
{
    public static function set($key, $value)
    {
        $search = DB::getInstance()->count("select count(*) as aggregate from configurations where name = '{$key}'");

        if (is_array($value)) {
            $value = json_encode($value);
        }

        if ($search > 0) {
            return DB::getInstance()->exec("update configurations set value = '{$value}' where name = '{$key}'");
        }

        return DB::getInstance()->exec("insert into configurations (name, value) values ('{$key}', '{$value}')");
    }

    public static function get($key, $default = null)
    {
        $result = DB::getInstance()->query("select value from configurations where name = '{$key}'");

        if ($result->isEmpty()) {
            return $default;
        }

        return $result->first()['value'];
    }

    public static function opp()
    {
        $opp = DB::getInstance()->query("select * from configurations where name like 'OPP_%'");

        $result = array();

        foreach ($opp->all() as $data) {
            $result[$data['name']] = $data['value'];
        }

        return new Collection($result);
    }
}

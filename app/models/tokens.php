<?php

namespace App\Models;

use DB;

class Tokens
{
    public static function getByUserId($userId, $payment)
    {
        $registrationIds = array();

        $result = DB::getInstance()->query("select registration_id from tokens where user_id = {$userId} and payment = '{$payment}'");

        foreach ($result->all() as $data) {
            $registrationIds[] = $data['registration_id'];
        }

        return $registrationIds;
    }

    public static function getRegistrationByUserId($userId, $payment)
    {
        $i = 0;
        $registrationIds = array();

        foreach (static::getByUserId($userId, $payment) as $registrationId) {
            $registrationIds[sprintf('registrations[%s].id', $i)] = $registrationId;

            $i++;
        }

        return $registrationIds;
    }

    public static function find($userId, $registrationId)
    {
        return DB::getInstance()->count("select count(*) as aggregate from tokens where user_id = {$userId} and registration_id = '{$registrationId}'");
    }

    public static function paypalTokens($userId)
    {
        return DB::getInstance()->query("select * from tokens where user_id = {$userId} and payment = 'paypal'");
    }
}

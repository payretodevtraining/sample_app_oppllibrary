<?php

namespace System;

require_once __DIR__.'/view.php';

class Response
{
    protected $content;

    public function __construct($content)
    {
        $this->content = $content;
    }

    public function send()
    {
        if ($this->content instanceof View) {
            $content = $this->content->render();
        } elseif (is_array($this->content)) {
            $content = json_encode($this->content);
        } else {
            $content = (string) $this->content;
        }

        echo $content;
    }
}

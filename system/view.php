<?php

namespace System;

use InvalidArgumentException;

class View
{
    protected $file;

    protected $master;

    protected $data = array();

    public function __construct($file, $data = array(), $master = null)
    {
        $this->data = $data;
        $this->file = $file;
        $this->master = $master;
    }

    public static function make($file, $data = array(), $master = null)
    {
        return new static($file, $data, $master);
    }

    public function render()
    {
        $view = $this->getView();

        extract($this->data);
        ob_start();
        require $view;

        return ob_get_clean();
    }

    protected function getView()
    {
        $viewFile = VIEW_DIR.'/'.$this->file.'.php';

        if (! file_exists($viewFile)) {
            throw new InvalidArgumentException("View not exists [{$viewFile}]");
        }

        if (is_null($this->master)) {
            $content = file_get_contents($viewFile);
        } else {
            $masterFile = VIEW_DIR.'/'.$this->master.'.php';

            if (! file_exists($masterFile)) {
               throw new InvalidArgumentException("View master not exists [{$this->master}]");
            }

            $masterContent = file_get_contents($masterFile);
            $childContent = file_get_contents($viewFile);

            $content = str_replace('{{content}}', $childContent, $masterContent);
        }

        $filename = STORAGE_DIR.'/app/views/'.md5($viewFile).'.php';

        file_put_contents($filename, $content);

        return $filename;
    }
}
